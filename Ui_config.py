# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\hachitz\wkspy\pyjiProTime\config.ui'
#
# Created: Mon Dec 17 12:13:13 2012
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(582, 149)
        self.formLayout = QtGui.QFormLayout(Dialog)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.ExpandingFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.txt_server_url = QtGui.QLineEdit(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.txt_server_url.setFont(font)
        self.txt_server_url.setObjectName(_fromUtf8("txt_server_url"))
        self.gridLayout.addWidget(self.txt_server_url, 0, 1, 1, 3)
        self.lbl_server_url = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_server_url.setFont(font)
        self.lbl_server_url.setObjectName(_fromUtf8("lbl_server_url"))
        self.gridLayout.addWidget(self.lbl_server_url, 0, 0, 1, 1)
        self.lbl_auth_usr = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_auth_usr.setFont(font)
        self.lbl_auth_usr.setObjectName(_fromUtf8("lbl_auth_usr"))
        self.gridLayout.addWidget(self.lbl_auth_usr, 1, 0, 1, 1)
        self.txt_auth_usr = QtGui.QLineEdit(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.txt_auth_usr.setFont(font)
        self.txt_auth_usr.setObjectName(_fromUtf8("txt_auth_usr"))
        self.gridLayout.addWidget(self.txt_auth_usr, 1, 1, 1, 1)
        self.lbl_auth_passwd = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_auth_passwd.setFont(font)
        self.lbl_auth_passwd.setTextFormat(QtCore.Qt.AutoText)
        self.lbl_auth_passwd.setObjectName(_fromUtf8("lbl_auth_passwd"))
        self.gridLayout.addWidget(self.lbl_auth_passwd, 1, 2, 1, 1)
        self.txt_auth_passwd = QtGui.QLineEdit(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.txt_auth_passwd.setFont(font)
        self.txt_auth_passwd.setEchoMode(QtGui.QLineEdit.Password)
        self.txt_auth_passwd.setObjectName(_fromUtf8("txt_auth_passwd"))
        self.gridLayout.addWidget(self.txt_auth_passwd, 1, 3, 1, 1)
        self.lbl_project = QtGui.QLabel(Dialog)
        self.lbl_project.setEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_project.setFont(font)
        self.lbl_project.setObjectName(_fromUtf8("lbl_project"))
        self.gridLayout.addWidget(self.lbl_project, 3, 0, 1, 1)
        self.cbo_project = QtGui.QComboBox(Dialog)
        self.cbo_project.setEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.cbo_project.setFont(font)
        self.cbo_project.setObjectName(_fromUtf8("cbo_project"))
        self.gridLayout.addWidget(self.cbo_project, 3, 1, 1, 3)
        self.lbl_info = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_info.setFont(font)
        self.lbl_info.setObjectName(_fromUtf8("lbl_info"))
        self.gridLayout.addWidget(self.lbl_info, 2, 1, 1, 3)
        self.formLayout.setLayout(0, QtGui.QFormLayout.SpanningRole, self.gridLayout)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.buttonBox.setFont(font)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Abort|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.buttonBox)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.txt_server_url, self.txt_auth_usr)
        Dialog.setTabOrder(self.txt_auth_usr, self.txt_auth_passwd)
        Dialog.setTabOrder(self.txt_auth_passwd, self.buttonBox)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "setup JIRA connection", None))
        self.lbl_server_url.setText(_translate("Dialog", "Server:", None))
        self.lbl_auth_usr.setText(_translate("Dialog", "User:", None))
        self.lbl_auth_passwd.setText(_translate("Dialog", "Password:", None))
        self.lbl_project.setText(_translate("Dialog", "Project:", None))
        self.lbl_info.setText(_translate("Dialog", "...", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

