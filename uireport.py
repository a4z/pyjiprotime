# -*- coding: utf-8 -*-

"""
Module implementing Dialog.
"""

from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignature , QUrl,  QDateTime,  QDate,  QTime 

from Ui_report import Ui_Dialog

import datetime
import json
import jira

class Dialog(QDialog, Ui_Dialog):
    """
    Class documentation goes here.
    """
    def __init__(self, parent = None):
        """
        Constructor
        """
        QDialog.__init__(self, parent)
        self.setupUi(self)
        # check initialise what must be called to work properly
        self.disableonrefresh = [ self.cbo_usr ,  self.date_oldest ,  self.btn_refresh ,  self.txt_data  ]


    @pyqtSignature("QString")
    def on_cbo_usr_textChanged(self, p0):
        """
        """
        print("on_cbo_usr_textChanged: " , p0 )

    @pyqtSignature("")
    def on_btn_refresh_clicked(self):
        """
        """
        self.request_projects()
     

    def initialize(self,  project_list,  project_selected):
        """ 
        """
        self.cbo_projects.clear() 
        self.cbo_projects.addItems(project_list)
        self.cbo_projects.setCurrentIndex(project_list.index(project_selected));
        
        self.cbo_usr.clear() ;
        self.cbo_usr.addItems(["-- ALL --"]) 
        self.cbo_usr.setCurrentIndex(0);

        now = datetime.datetime.now() 
        yesterday = datetime.datetime.now() - datetime.timedelta(days = 1)
        #from is the higher date, to the lower on
        #QDateTime.__init__ (self, int y, int m, int d, int h, int m, int s = 0, int msec = 0, int timeSpec = 0)
        self.date_newest.setDateTime( QDateTime(now.year,  now.month,  now.day,  now.hour,  now.minute) )        
        self.date_oldest.setDateTime( QDateTime(yesterday.year,  yesterday.month,  yesterday.day,  0,  0) )
        
        maxdt = QDateTime(QDate(yesterday.year,  yesterday.month,  yesterday.day),  QTime(0, 0))
        mindate = yesterday - datetime.timedelta(days = 3)
        mindt = QDateTime(QDate(mindate.year,  mindate.month,  mindate.day),  QTime(0, 0))
        self.date_oldest.setMaximumDateTime(maxdt)
        #self.date_oldest.setMinimumDateTime(mindt) do this in init.
        
        self.jql_newest = self.date_newest.dateTime()
        self.jql_oldest = self.date_newest.dateTime() # refresh 1st time  in init, then self.date_oldest.date()

        self.issues = {}
        self.report = {}
        self.worklogs = {}    
        self.knownusers = []

        self.request_projects()


    def showProgress(self,  msg = None):
        pass 

    # todo ,dupple issues on changing_/extending data aproble,....?? 
    def request_projects(self):
        if not self.date_oldest.date().toString("yyyy/MM/dd") < self.jql_oldest.toString("yyyy/MM/dd") :
            print ("no changes in date")
            self.makeReport()
            return None
            
        for ctr in self.disableonrefresh : ctr.setEnabled( False ) 
       
        self.jql_newest = self.jql_oldest
        self.jql_oldest = self.date_oldest.dateTime()
        
        self.date_oldest.setMinimumDateTime(self.jql_oldest.addDays(-3))
        
        # TODO, set new range for date field...
        
        #prepair jql query
        jqltmplt = "project='{project}' AND updated <'{newest}' AND updated >='{oldest}'"
        jql = jqltmplt.format(project=self.cbo_projects.currentText(), 
                              oldest=self.jql_oldest.toString("yyyy/MM/dd hh:mm"), 
                              newest=self.jql_newest.toString("yyyy/MM/dd hh:mm") )   
        print (jql)
    
        queryfields = ["key", "summary", "issuetype", "parent"]
        query = { "jql" : jql , "startAt" : 0 , "maxResults" : 30 , "fields" : queryfields }         
        
        #requesthandler, 
        def projectSearchHandler(reply):
            #add issue to issu list, handels also some logic with parent project ...
            def addIssue(issue):
                key = issue["key"]
                fields = issue["fields"]
                summary = fields["summary"]
                parent = fields.get("parent")
                parentkey = None 
                if parent != None:
                    parentfields = parent["fields"]
                    parentkey = parent["key"]
                    #if not self.issues.has_key(parentkey) :
                    if not parentkey in self.issues :   
                        self.issues[parentkey] = { "key": parentkey , "summary" : parentfields["summary"], "parent" : None }
                    
                if key not in self.issues: 
                    self.issues[key] = { "key": key , "summary" : summary, "parent" : parentkey ,  "worklog": True}
                else:
                    if "worklog" not in self.issues[key] : self.issues[key]["worklog"] = True
            
            def handler():                
                ok,  msg = jira.check_reply(reply)
                if not ok: 
                    QMessageBox.critical(self,  "Requesting projects failed!",  msg)                 
                    return None
                # TODO add exception hanlder if there are no entries to process...    
                projects = json.loads(reply.readAll().data().decode())
                qrange = projects["startAt"] , projects["maxResults"], projects["total"] 

                for issue in projects["issues"]:
                    addIssue(issue)
                
                reply.deleteLater()
                # if more projects available, call again 
                if not qrange[0] + qrange[1] > qrange[2]:
                    query["startAt"] = qrange[0] + qrange[1]  
                    jira.search(query,  projectSearchHandler)
                    
                else:
                    # todo, show some progress ...
                    self.fetchWorklogs()
            return handler        
            
        jira.search(query,  projectSearchHandler)

    
    def fetchWorklogs(self):
        """ { key : { summary : val, parent: val } }
            { key : { usr : val, started: val , timesec: val } }
        """ 
        
        # process all issues that have a worklog key ...
        
        def addWorklog(id,  key,  usr,  started,  timesec):
            sdate = started.split("T")[0] #prepair the date for used format
            # assign worklog data, simply always insert/overwrite  
            self.worklogs[id] = { "key": key , "usr" : usr, "started" : sdate , "timesec" : timesec }   
            if usr not in self.knownusers:
               self.knownusers.append(usr)
            

        def processWorks(works, callback):
            if len(works) > 0 :
                issue = works.pop(0)
                jira.requestWorklog(issue, callback(works, issue))   
            else:
                self.makeReport()
                
        
        def replyHandler(works,  issue):
            def closureFW(reply):
                def handler():
                    if jira.check_reply(reply):
                        worklogs = json.loads(reply.readAll().data().decode())
                        for work in worklogs["worklogs"]:
                            addWorklog(work["id"], issue, work["author"]["name"], work["started"] , work["timeSpentSeconds"])
                        
                    reply.deleteLater()
                    processWorks( works, replyHandler ) # call again ...
                return handler
            return closureFW
        
        works = []
        for issuekey in self.issues:
            if "worklog" in self.issues[issuekey]:
                works.append(issuekey)
        #print "have " ,  len(works),  " issues with worklog to process"
        
        processWorks( works, replyHandler )
        
        
    def makeReport(self):
        """ self.issues[key] = { "key": key , "summary" : summary, "parent" : parentkey ,  "worklog": True}
            self.worklogs = [{ "key": key , "usr" : usr, "started" : started , "timesec" : timesec } ]
            
            date { key_val : { summary : val , workmap : {user : time} , subtasks :  
                 { key_val : { summary : val , workmap : {user : time} } 
                 
                 
        """ 
        self.report = {}

        def dicdefault(d,  k,  v):
            r = d.get(k)
            if r == None : 
                r = d[k]=v
            return r


        def getWorkMap(daylog, issue_id):
            issue = self.issues[issue_id]
            parent_id = issue["parent"]
            
            if parent_id != None:
                daylystory = dicdefault(daylog, parent_id, { "summary" : self.issues[parent_id]["summary"] , "workmap" : {} , "subtasks" : { } } )  
                subtask = dicdefault(daylystory["subtasks"], issue_id, { "summary" : self.issues[issue_id]["summary"] , "workmap" : {} } )
                return  subtask["workmap"]
            else:
                daylystory = dicdefault(daylog, issue_id, { "summary" : self.issues[issue_id]["summary"] , "workmap" : {} , "subtasks" : { } } )
                return  daylystory["workmap"]
        
        for wid, worklog in self.worklogs.items():
            daylog = dicdefault(self.report , worklog["started"],  {} )
            workmap = getWorkMap(daylog,  worklog["key"])
            
            timesec = workmap.get(worklog["usr"], 0)
            timesec+= worklog["timesec"]
            workmap[worklog["usr"]] = timesec


        self.txt_data.clear()
        jstr = json.dumps(self.report,  sort_keys=True,  indent = 4)
        print (jstr)
        
        selecteusr = self.cbo_usr.currentText()
        self.cbo_usr.clear()
        self.cbo_usr.addItem("-- ALL --")
        self.cbo_usr.addItems(sorted(self.knownusers))
        self.cbo_usr.setCurrentIndex(0)
        for i in range(1,self.cbo_usr.count()):
            if self.cbo_usr.itemText(i) == selecteusr:
                self.cbo_usr.setCurrentIndex(i)
        self.cbo_usr.setEnabled(True) 
        
        for ctr in self.disableonrefresh : ctr.setEnabled( True ) 
        
        def has_task_usr(tasklist,  usr):
            if tasklist["workmap"].get(usr)!=None:
                return True
            return False
        
        def has_story_usr(storydic,  usr):
            if storydic["workmap"].get(usr) != None:
                return True
            for tasklist in storydic["subtasks"].itervalues():
                if has_task_usr(tasklist,  usr):
                    return True
            return False
        
        def have_stories_usr(stories,  usr): 
            for storydic in stories.itervalues():
                if has_story_usr(storydic,  usr): 
                    return True
            return False
            
        
        for day in sorted(self.report):
            if day < self.date_oldest.date().toString("yyyy-MM-dd"): 
                continue
            
            if self.cbo_usr.currentIndex ()  > 0: 
                curusr = str(self.cbo_usr.currentText())
            else : 
                curusr = None
                
            
            stories = self.report[day]
            if curusr != None:
                if not have_stories_usr(stories,  curusr):
                    continue
            
            self.txt_data.append(day)
            
            for storyid in sorted(stories):
                story = stories[storyid]
                if curusr != None:
                    if not has_story_usr(story,  curusr):
                        continue                
                self.txt_data.append("  "+ storyid + ": " +story["summary"])
                for u, t in story["workmap"].items():
                    self.txt_data.append("      "+ u + ": " + str(datetime.timedelta(seconds=t)))
                for taskid, task in story["subtasks"].items():
                    if curusr != None:
                        if not has_task_usr(task,  curusr):
                            continue                                    
                    self.txt_data.append("    "+ taskid + ": " +task["summary"])
                    for u, t in task["workmap"].items():
                        self.txt_data.append("      "+ u + ": " + str(datetime.timedelta(seconds=t)))
                
                self.txt_data.append("------------------------------------------------------")    
            self.txt_data.append("====================================================")
            self.txt_data.append("")




if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication    
    app = QApplication(sys.argv)
    d = Dialog()
    d.initialize(["Something","Database SDK",  "Else"], "Database SDK") 
    d.show()
    sys.exit(app.exec_())
