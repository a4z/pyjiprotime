
from PyQt4.QtCore import  QUrl 
from PyQt4.QtNetwork import QNetworkAccessManager , QNetworkRequest 

import base64
import json 

class Error(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class User(object):
    _name = None
    _password = None 
    _mail = None
    _displayName = None 
    
    def __init__(self, name = None ,  password = None):
        if name != None : User._name = name
        if password != None :  User._password = password
        
    @property
    def name(self):
        """user name for jira"""       
        return User._name
        
    @property
    def password(self):
        """user password for jira"""       
        return User._password


    def getAuth(self):
        """return username, password if set or raise exception if incomplete"""
        if User._name == None or User._password == None:
            raise Error("incomplete user data")
        return User._name,  User._password
  
  
class Server(object):
    _manager = None
    _url = None 
    
    def __init__(self,  url = None):
        if Server._manager == None : Server._manager = QNetworkAccessManager()
        if url : Server._url = url 
    
    @property
    def url(self):
        return Server._url

    @property
    def qnetmanager(self):
        """returns the QNetworkAccessManager"""       
        return Server._manager    

    def request(self,  restcall):
        """creates a QNetworkRequest, only the rest part of the call needs to be passed into this function,
            the url url including /rest/api/2/ is prepended.
        """
        if Server._url is None:
            raise Error("server url missing")
            
        restapicall = "{url}/rest/api/2/{rest}".format(url=Server._url,  rest=restcall)
        qrequest = QNetworkRequest(QUrl(restapicall))
        usr,  pwd = User().getAuth()
        #authdata = base64.encodestring('%s:%s' % (usr, pwd))[:-1]
        authdata = base64.encodestring( ('%s:%s' % (usr, pwd)).encode('utf-8') )[:-1].decode()
        qrequest.setRawHeader("Authorization", "Basic {auth}".format(auth=authdata) );
        qrequest.setRawHeader("Content-Type",  "application/json; charset=utf-8")
        return qrequest    

    def GET(self, restcall):
        request = self.request(restcall)
        reply = Server._manager.get(request)
        return request, reply   

    def POST(self, restcall, data):
        request = self.request(restcall)
        reply = Server._manager.post(request, data)
        return request, reply   
   


def GET(restcall):
    request, reply = Server().GET(restcall)
    return reply 

def POST(restcall, data):
    request, reply = Server().POST(restcall,  data)
    return reply     


def createConnection(url,  username,  password):
    User(username,  password)
    return Server(url)
    
# finished is afunction with 1 argument, the reply
def requestLogin(finished,  uphandler = None,  downhandler = None):
    reply = GET( "user?username={username}".format(username = User().name) )    
    if finished != None: reply.finished.connect(finished(reply))   
    if uphandler != None: reply.uploadProgress.connect(uphandler)
    if downhandler != None: reply.downloadProgress.connect(downhandler)   
    return reply
    
def requestProjects(finished,  uphandler = None,  downhandler = None):
    reply = GET( "project" )    
    if finished != None: reply.finished.connect(finished(reply))   
    if uphandler != None: reply.uploadProgress.connect(uphandler)
    if downhandler != None: reply.downloadProgress.connect(downhandler)   
    return reply    
    
def search(query,  finished,  uphandler = None,  downhandler = None):
    reply = POST("search", json.dumps(query) )
    if finished != None: reply.finished.connect(finished(reply))   
    if uphandler != None: reply.uploadProgress.connect(uphandler)
    if downhandler != None: reply.downloadProgress.connect(downhandler)       
    return reply
    
def requestWorklog(issue, finished,  uphandler = None,  downhandler = None): 
    reply = GET("issue/{issue}/worklog".format(issue = issue) )   
    if finished != None: reply.finished.connect(finished(reply))   
    if uphandler != None: reply.uploadProgress.connect(uphandler)
    if downhandler != None: reply.downloadProgress.connect(downhandler)   
    return reply    


def check_reply(reply):
    """ check http error code, 
        return true/false None/Some default message
    """
    #if reply.error() == 0 and reply.attribute(QNetworkRequest.HttpStatusCodeAttribute).toInt() == (200, True):
    if reply.error() == 0 and reply.attribute(QNetworkRequest.HttpStatusCodeAttribute) == 200 :
        return True ,  None 
    else:
        msg = "!!! Connetion faild !!! \n"
        #msg+= "reply.error() == {rerror} , HttpStatusCode = {httpcode}".format(rerror = reply.error() ,  httpcode = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute).toInt())
        msg+= "reply.error() == {rerror} , HttpStatusCode = {httpcode}".format(rerror = reply.error() ,  httpcode = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute))
        msg+="\nNote: if you have submitted a wrong password it might be required to use the login page to reenable login"
        print (reply.readAll())
        return False,  msg
        
