# -*- coding: utf-8 -*-

"""
Module implementing Dialog.
"""

from PyQt4.QtGui import QDialog , QMessageBox
from PyQt4.QtCore import pyqtSignature

from Ui_config import Ui_Dialog

import uireport

import json
import jira


class Dialog(QDialog, Ui_Dialog):
    """
    Class documentation goes here.
    """
    def __init__(self, parent = None):
        """
        Constructor
        """
        QDialog.__init__(self, parent)
        self.setupUi(self)
        
        try: # if possible assign some defaulf values 
           with open('uilogin.defaults') as f: 
               self.defaults = json.load(f) 
               self.txt_server_url.setText(self.defaults.get("server",  ""))
               self.txt_auth_usr.setText(self.defaults.get("username",  ""))
        except :
           print("no defaultvalues used",  e)
        # put focus on the right place 
        if self.txt_server_url.text()=="":
            self.txt_server_url.setFocus()
        elif self.txt_auth_usr.text()=="":
            self.txt_auth_usr.setFocus()
        elif self.txt_auth_passwd.text()=="":
            self.txt_auth_passwd.setFocus()        
        
        # some helpers to enable / disable simpler
        self.login_widgets = [self.txt_server_url, self.txt_auth_usr, self.txt_auth_passwd, 
                                 self.lbl_server_url, self.lbl_auth_usr, self.lbl_auth_passwd  ]    
        
        self.project_widgets = [self.lbl_project,  self.cbo_project]
        
        # some init stuff/values 
        for  w in self.project_widgets:
           w.hide() 
        
        self.lbl_info.setText("")
        self.accept_action = self.login_accepted;  #default action for btn ok click
        
        self.progressval = 0
    
    
    
    def set_progress_info(self,  msg,  v = None ):
        """display some progress, show given msg in info label and append some changing chars"""
        pntxt= [
        "    -    ", "   ---   ",  "  -----  ",
        "---------", 
        "  -----  ", "   ---   ",  "    -    "]
        
        if v != None : self.progressval = v
        else:
            self.progressval+=1
            if not self.progressval < len(pntxt):
                self.progressval = 0
        self.lbl_info.setText("{msg} {bar}".format(msg=msg, bar=pntxt[self.progressval]))
    
    
    
    @pyqtSignature("QString")
    def on_cbo_project_textChanged(self, p0):
        """
        Slot documentation goes here.
        """
        print("cbo_txtChanged, " ,  p0 )
    
    
    
    def login_accepted(self):
        
        for txtbox in [ self.txt_server_url,  self.txt_auth_usr, self.txt_auth_passwd ]: 
            if txtbox.text() == "":
                txtbox.setFocus()
                QMessageBox.critical(self,  "Value missing",  "Please check required login information")
                return None         


        def progresscallback(msg):
            """use this for displaying some pure man's progress bar for a request"""
            def callback(up,  down):
                self.set_progress_info(msg)
            return callback
        
        def projectHandler(reply):
            """if login was successfull progressjects are requested, this is the request handler therefore"""
            def handler():        
                ok,  msg = jira.check_reply(reply)
                if not ok: 
                    QMessageBox.critical(self,  "Requesting projects failed!",  msg)                 
                else:
                    self.cbo_project.clear(); # clear project list
                    projectnames = [] #create sorted list of project names
                    for project in json.loads(reply.readAll().data().decode()):
                        projectnames.append(project["name"])
                    projectnames.sort() 
                    self.cbo_project.addItems(projectnames) # append list to combo box
                    try: # select the default if possible
                        self.cbo_project.setCurrentIndex( projectnames.index(self.defaults.get("project")) );
                    except:
                        pass #if default does not exist or is None, simply do nothing   
                    for  w in self.project_widgets: #display the project widgets, 
                       w.show()
                    self.lbl_info.hide()
                    self.cbo_project.setFocus()
                    for  w in self.login_widgets: #and disable connect widgets
                       w.setEnabled( False ) 
                    self.set_progress_info("",  0) # clear info range
                    self.accept_action = self.project_accepted #set button ok action
                    self.setWindowTitle("select project")
            return handler
  
        def loginHander(reply):
            """runs for the login requst, on success projects will be requested"""
            def handler():
                ok,  msg = jira.check_reply(reply)
                if not ok: 
                    QMessageBox.critical(self,  "Login failed!",  msg) 
                else: #if login was ok, proceed with requesting a list of projects 
                    infomsg = "request projects"
                    self.set_progress_info(infomsg,  0)
                    jira.requestProjects(projectHandler,  downhandler = progresscallback(infomsg))
                    
                reply.deleteLater()
            return handler        
        
        #let's go 
        infomsg = "request login"
        self.set_progress_info(infomsg,  0)
        jira.createConnection(self.txt_server_url.text(),  self.txt_auth_usr.text(), self.txt_auth_passwd.text() )
        jira.requestLogin(loginHander,  downhandler = progresscallback(infomsg))
    
    
    
    def project_accepted(self):
        #QMessageBox.critical(self,  "TODO!",  "show other dialog")  
        if not hasattr(self, 'reportdialog'):
            self.reportdialog = uireport.Dialog(self)
        projects = [ self.cbo_project.itemText(i) for i in range(0,self.cbo_project.count()) ]
        self.reportdialog.initialize(projects ,  self.cbo_project.currentText())
        ok = self.reportdialog.exec_()

    @pyqtSignature("")
    def on_buttonBox_accepted(self):
        """ call the actual ok handler 
        """
        self.accept_action() 
    
    @pyqtSignature("")
    def on_buttonBox_rejected(self):
        """ good bye 
        """
        self.close()
        
        
if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication    
    app = QApplication(sys.argv)
    d = Dialog()
    d.show()
    sys.exit(app.exec_())
