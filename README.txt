This it the result of a long weekend personal hackathon with the target to 
do something with python and pyQt.

and this is a quick description and the help.

pyjiProTime is a Jira (Atlassian) GUI client to view worklogs of a project.

There is a log in dialog for the web url, username and password.

After successfully connected to Jira you can select a project.
On the next dialog you can see the worklog entries for this project.

First you will see the work logs from the last 3 days.
If wanted, you can navigate back in time in 3 day steps.

The work log can be filtered for persons.


Limitations:
there is no guarantee that worklogs that have been dated into the future will
be shown.
the Jira REST API makes it unfortunately somehow difficult to work with
worklogs.

Worklogs added to a data that is shown while the dialog is open will not
refresh, you must reopen the dialog.
This is to not cause to much traffic on the Jira server.

As said, this project is a result of a personal hackathon, so the code qualityt 
is proof of concept, no docu, tests and it should just work for the moment.

Nevertheless, this is a working app that can be useful if you own a Jira project
or want to review you personal worklogs.

 

Hints:
you can add a file with the name uilogin.defaults into the project directory
this file has to look like this.
{
    "project": "Default selectd project", 
    "server": "https://your.jira.server.info", 
    "username": "your user name" 
}
the values will be used as default values, each value optional.


python main.py will run the project.





