# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\hachitz\wkspy\pyjiProTime\report.ui'
#
# Created: Mon Dec 17 12:13:13 2012
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(623, 598)
        font = QtGui.QFont()
        font.setPointSize(10)
        Dialog.setFont(font)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.txt_data = QtGui.QTextEdit(Dialog)
        self.txt_data.setObjectName(_fromUtf8("txt_data"))
        self.gridLayout_2.addWidget(self.txt_data, 1, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.cbo_usr = QtGui.QComboBox(Dialog)
        self.cbo_usr.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cbo_usr.sizePolicy().hasHeightForWidth())
        self.cbo_usr.setSizePolicy(sizePolicy)
        self.cbo_usr.setFrame(True)
        self.cbo_usr.setObjectName(_fromUtf8("cbo_usr"))
        self.gridLayout.addWidget(self.cbo_usr, 2, 1, 1, 1)
        self.lbl_usr = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_usr.sizePolicy().hasHeightForWidth())
        self.lbl_usr.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_usr.setFont(font)
        self.lbl_usr.setObjectName(_fromUtf8("lbl_usr"))
        self.gridLayout.addWidget(self.lbl_usr, 2, 0, 1, 1)
        self.lbl_date_from = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_date_from.sizePolicy().hasHeightForWidth())
        self.lbl_date_from.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_date_from.setFont(font)
        self.lbl_date_from.setObjectName(_fromUtf8("lbl_date_from"))
        self.gridLayout.addWidget(self.lbl_date_from, 1, 0, 1, 1)
        self.btn_refresh = QtGui.QPushButton(Dialog)
        self.btn_refresh.setObjectName(_fromUtf8("btn_refresh"))
        self.gridLayout.addWidget(self.btn_refresh, 2, 3, 1, 1)
        self.date_newest = QtGui.QDateTimeEdit(Dialog)
        self.date_newest.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.date_newest.sizePolicy().hasHeightForWidth())
        self.date_newest.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.date_newest.setFont(font)
        self.date_newest.setCurrentSection(QtGui.QDateTimeEdit.YearSection)
        self.date_newest.setCalendarPopup(True)
        self.date_newest.setObjectName(_fromUtf8("date_newest"))
        self.gridLayout.addWidget(self.date_newest, 1, 1, 1, 1)
        self.lbl_date_to = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_date_to.sizePolicy().hasHeightForWidth())
        self.lbl_date_to.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_date_to.setFont(font)
        self.lbl_date_to.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.lbl_date_to.setObjectName(_fromUtf8("lbl_date_to"))
        self.gridLayout.addWidget(self.lbl_date_to, 1, 2, 1, 1)
        self.cbo_projects = QtGui.QComboBox(Dialog)
        self.cbo_projects.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cbo_projects.sizePolicy().hasHeightForWidth())
        self.cbo_projects.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.cbo_projects.setFont(font)
        self.cbo_projects.setEditable(False)
        self.cbo_projects.setObjectName(_fromUtf8("cbo_projects"))
        self.gridLayout.addWidget(self.cbo_projects, 0, 1, 1, 3)
        self.lbl_project = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_project.sizePolicy().hasHeightForWidth())
        self.lbl_project.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lbl_project.setFont(font)
        self.lbl_project.setObjectName(_fromUtf8("lbl_project"))
        self.gridLayout.addWidget(self.lbl_project, 0, 0, 1, 1)
        spacerItem = QtGui.QSpacerItem(218, 20, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 4, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 4, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 2, 4, 1, 1)
        self.date_oldest = QtGui.QDateEdit(Dialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.date_oldest.setFont(font)
        self.date_oldest.setDateTime(QtCore.QDateTime(QtCore.QDate(2000, 1, 1), QtCore.QTime(0, 0, 0)))
        self.date_oldest.setMinimumDateTime(QtCore.QDateTime(QtCore.QDate(1756, 9, 14), QtCore.QTime(0, 0, 0)))
        self.date_oldest.setCurrentSection(QtGui.QDateTimeEdit.YearSection)
        self.date_oldest.setCalendarPopup(True)
        self.date_oldest.setObjectName(_fromUtf8("date_oldest"))
        self.gridLayout.addWidget(self.date_oldest, 1, 3, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.cbo_projects, self.date_newest)
        Dialog.setTabOrder(self.date_newest, self.date_oldest)
        Dialog.setTabOrder(self.date_oldest, self.cbo_usr)
        Dialog.setTabOrder(self.cbo_usr, self.btn_refresh)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "time report", None))
        self.lbl_usr.setText(_translate("Dialog", "User:", None))
        self.lbl_date_from.setText(_translate("Dialog", "From:", None))
        self.btn_refresh.setText(_translate("Dialog", "Refresh", None))
        self.date_newest.setDisplayFormat(_translate("Dialog", "yyyy-MM-dd hh:mm ", None))
        self.lbl_date_to.setText(_translate("Dialog", "To:", None))
        self.lbl_project.setText(_translate("Dialog", "Project:", None))
        self.date_oldest.setDisplayFormat(_translate("Dialog", "yyyy-MM-dd hh:mm ", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

