


def main():
    import sys
    import uiconfig
    from PyQt4.QtGui import QApplication    
    app = QApplication(sys.argv)
    d = uiconfig.Dialog()
    d.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
